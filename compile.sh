#! /bin/sh

render(){
    pdflatex -output-directory=./intermediate Summary.tex
}

render
biber --output-directory ./intermediate  Summary
render
render
mv ./intermediate/*.pdf ./pdf/
